<!DOCTYPE php>
<php>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/all.min.css" />
        <link rel="stylesheet" href="css/style.css">
        <title>Blood Donors</title>
        <style>
            * {
                margin: 0;
                padding: 0;
                transition: all 0.2s
            }

            body {
                transition: all .5s;
                background-color: #F8F8F8;
            }

            .navbar {
                background-color: #fff !important;
                box-shadow: 0px -7px 11px 1px #000;
            }

            @media (min-width: 1199px) {
                .navbar-nav {
                    margin-left: 5%;
                }
            }

            .navbar-light .navbar-nav .nav-link {
                font-size: 17px;
                font-weight: 600;
            }

            .navbar-light .navbar-nav .nav-item {
                margin-right: 15px
            }

            .navbar-light .navbar-nav .active>.nav-link {
                color: #FA6703;
            }

            .right-icons {
                margin-right: 10px
            }

            .right-icons span {
                margin-right: 20px
            }

            .notify-icon,
            .profile-icon,
            .search-icon {
                font-size: 18px;
            }

            .notify-icon {
                position: relative;
            }

            .notify-icon .notify-mark {
                position: absolute;
                top: -4px;
                left: 5px;
                background-color: #FA6703;
                width: 15px;
                height: 15px;
                border-radius: 50%;
            }

            .notify-icon .notify-mark .notify-mark-inner {
                position: absolute;
                top: -3px;
                left: 3.5px;
                font-size: 13px;
                color: #fff;
                font-weight: 600;
            }

            /*.....................................................................................*/

            .form-register .register input {
                padding: 25px 10px;
                border-radius: 5px;
                outline: none
            }

            .form-register .register select {
                padding: 14px 10px;
                border-radius: 5px;
                outline: none
            }

            .form-register .register input[type="submit"],
            .form-register .register input[type="number"] {
                padding: 0 10px
            }

            .form-register .register input[type="number"] {
                height: 55px
            }

            .btn {
                margin-top: 10px;
                background-color: #fa6703;
                color: #F8F8F8;
                text-align: center;
                border-radius: 23px;
                font-weight: bold;
            }

            .btn:hover {
                color: #000
            }

            .form-register .register .first {
                margin-bottom: 20px
            }

            /*...........................................................................................*/

            #footer {
                background-color: #fff;
                padding-top: 60px;
                padding-bottom: 5px;
            }

            #footer .social-icons {
                padding-bottom: 5px
            }

            #footer .social-icons span:hover {
                color: #FA6703
            }

            #footer .social-icons span {
                cursor: pointer;
                transition: all 0.1s !important;
                font-size: 20px;
                margin-right: 30px;
                color: #000;
            }

            #footer .short-links ul li {
                font-size: 20px
            }

            #footer .short-links .list-inline .list-inline-item {
                margin-right: 30px;
                cursor: pointer;
            }

            #footer .short-links .list-inline .list-inline-item.active {
                color: #FA6703
            }

            #footer .short-links .list-inline .list-inline-item:hover {
                text-decoration: underline;
                color: #FA6703
            }
        </style>
    </head>

    <body>
        <section id="navbar">
            <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
                <a class="navbar-brand" href="#" style="font-size: 20px;
                    color: #080;
                    font-weight: 600;">
                    <span style="color: #FA6703">BLOOD</span><span style="color: gray">DONORS</span>
                    <i class="fab fa-pagelines"></i></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="offset-md-3 text-center collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="support.php">Support</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about.php">About Us</a>
                        </li>
                    </ul>
                    <div class="right-icons">
                        <span class="notify-icon">
                            <span class="notify-mark">
                                <span class="notify-mark-inner">3</span>
                            </span>
                            <i class="fa fa-bell"></i>
                        </span>
                        <span class="profile-icon">
                            <a style="color: #000" href="login.php"><i class="fa fa-user"></i></a>
                        </span>
                        <span class="search-icon">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </div>
            </nav>

        </section>
        <!--..........................................................................................-->
        <section style="padding: 50px 0">
            <?php
            include "connect.php";
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $PName      = $_POST['PName'];
                $DName      = $_POST['DName'];
                $City       = $_POST['City'];
                $Address    = $_POST['Address'];
                $Email      = $_POST['email'];
                $Blood      = $_POST['Blood'];
                $CBlood     = $_POST['CBlood'];
                $PNum       = $_POST['PNum'];
                $Gender     = $_POST['Gender'];
                $NationalID = $_POST['NationalID'];
                $CNI        = $_POST['CNI'];

                //Valdiate The Form
                global $formErors;
                $formErors = array();
                if (empty($PName)) {
                    $formErors[] = " Patient Name Can't Be <strong>Empty!</strong>";
                }
                if (empty($DName)) {
                    $formErors[] = " Doctor Name Can't Be <strong>Empty!</strong>";
                }
                if (empty($City)) {
                    $formErors[] = " City Can't Be <strong>Empty!</strong>";
                }
                if (empty($Address)) {
                    $formErors[] = " Address Can't Be <strong>Empty!</strong>";
                }
                if (empty($Gender)) {
                    $formErors[] = " Gender Can't Be <strong>Empty!</strong>";
                }
                if (empty($Address)) {
                    $formErors[] = " Blood Can't Be <strong>Empty!</strong>";
                }
                if (empty($Blood)) {
                    $formErors[] = " Blood Can't Be <strong>Empty!</strong>";
                }
                if (empty($CBlood)) {
                    $formErors[] = " Unit Blood Can't Be <strong>Empty!</strong>";
                }
                if (empty($PNum)) {
                    $formErors[] = " Phone Number Can't Be <strong>Empty!</strong>";
                }
                if (empty($NationalID)) {
                    $formErors[] = " NationalID Can't Be <strong>Empty!</strong>";
                }
                if (empty($CNI)) {
                    $formErors[] = " confirm national id Can't Be <strong>Empty!</strong>";
                }
                if ($NationalID != $CNI) {
                    $formErors[] = " confirm national id <strong>Not Same</strong> national id";
                }

                foreach ($formErors as $error) {
                    echo "<div class=' text-center alert alert-danger' style='width: 400px; padding: 8px;  font-size: 17px !important; margin: 5px auto '>" . $error . "</div>";
                }
                if (empty($formErors)) {
                    $stmt = $con->prepare("INSERT INTO need_blood ( patient_name, Doctor_name, city, Address, Email, select_gender, phone_num, select_blood , unit_blood, national_id, confirm_national_id) VALUES ( ?, ?, ? , ?, ?, ? ,?, ?, ?, ?,?) ");
                    $stmt->execute(array(
                        $PName, $DName, $City, $Address, $Email, $Gender, $PNum, $Blood, $CBlood, $NationalID, $CNI
                    ));

                    $count = $stmt->rowCount();
                    if ($count > 0) {
                        echo "<div class=' text-center alert alert-success' style ='width: 700px; font-size: 21px; margin: 30px auto 5px'>Sucess Thanks For Your Time</div>";
                    }
                }
            }

            ?>
            <h2 class="text-center" style="font-weight: 700;color: #FA6703 ;margin:50px 0 ">NEED BLOOD</h2>
            <div class="form-register" style="
                    background-color: #fff;
                    border: 2px solid #ddd;
                    border-radius: 10px;
                    width: 80%;
                    margin-left: auto;
                    margin-right: auto;
                    padding: 40px;
        ">
                <form class="register " action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">

                    <div class="row">
                        <div class="col-md-6 first">
                            <input type="text" name="PName" class="form-control" placeholder="Patient Name" />
                        </div>
                        <div class="col-md-6 first">
                            <input type="text" name="DName" class="form-control" placeholder="Doctor Name" />
                        </div>
                        <div class="col-md-6 first">
                            <input type="text" name="City" class=" form-control" placeholder="Typing your City" />
                        </div>
                        <div class="col-md-6 first">
                            <input type="text" name="Address" class="form-control" placeholder="Address" />
                        </div>
                        <div class="col-md-4 first">
                            <input type="email" name="email" class="form-control" placeholder="Email" />
                        </div>
                        <div class="col-md-4 first">
                            <select name="Blood" class="form-control input-sm" size="1">
                                <option name="Blood" value="">Select Blood</option>
                                <option name="Blood" value="A+">A+</option>
                                <option name="Blood" value="B+">B+</option>
                                <option name="Blood" value="O+">O+</option>
                                <option name="Blood" value="AB+">AB+</option>
                                <option name="Blood" value="A1+">A1+</option>
                                <option name="Blood" value="A2+">A2+</option>
                                <option name="Blood" value="A1B+">A1B+</option>
                                <option name="Blood" value="A2B+">A2B+</option>
                                <option name="Blood" value="A-">A-</option>
                                <option name="Blood" value="B-">B-</option>
                                <option name="Blood" value="O-">O-</option>
                                <option name="Blood" value="AB-">AB-</option>
                                <option name="Blood" value="A1-">A1-</option>
                                <option name="Blood" value="A2-">A2-</option>
                                <option name="Blood" value="A1B">A1B-</option>
                                <option name="Blood" value="A2B">A2B-</option>
                                <option name="Blood" value="A2B">Bombay o+</option>
                                <option name="Blood" value="A2B">Bombay o-</option>
                            </select>
                        </div>
                        <div class="col-md-4 first num">
                            <input type="number" name="CBlood" class="form-control" placeholder="Unit Of Blood" min="1" />
                        </div>

                        <div class="col-md-6 first">
                            <select id="gen" name="Gender" class="form-control input-sm" size="1">
                                <option value="">Select Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-6 first">
                            <input type="tel" name="PNum" class="form-control" placeholder="Phone Number" />
                        </div>
                        <div class="col-md-6 first">
                            <input type="text" name="NationalID" class="form-control" placeholder="National ID" pattern="[0-9]{13}" />
                        </div>
                        <div class="col-md-6 first">
                            <input type="text" name="CNI" class="form-control" placeholder="Confirm National ID" pattern="[0-9]{13}" />
                        </div>
                        <div class="text-center col-md-6 offset-md-3">
                            <input type="submit" value="SEND" class="form-control btn btn-lg">
                        </div>
                    </div>
                </form>

            </div>
        </section>
        <!--..........................................................................................-->
        <!--..........................................................................................-->
        <hr style="margin-bottom: 0;margin-top: 0">
        <section id="footer">
            <div class="short-links text-center">
                <ul class="list-inline">
                    <li class="list-inline-item">Home</li>
                    <li class="list-inline-item">Support</li>
                    <li class="list-inline-item">Privacy</li>
                    <li class="list-inline-item">Terms</li>
                </ul>
            </div>
            <div class="text-center social-icons">
                <span>
                    <i class="fab fa-facebook-square"></i>
                </span>
                <span>
                    <i class="fab fa-twitter-square"></i>
                </span>
                <span>
                    <i class="fab fa-youtube"></i>
                </span>
                <span>
                    <i class="fab fa-behance-square"></i>
                </span>
            </div>
            <hr style="margin-bottom: 5px;">
            <div class="text-center">Copyright &copy; 2019 All Rights Reserved.</div>
        </section>
        <script src="js/all.min.js"></script>
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>

</php>