-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2019 at 11:47 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blood`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `email`, `password`) VALUES
(1, 'hassan@gmail.com', '7C4A8D09CA3762AF61E59520943DC26494F8941B');

-- --------------------------------------------------------

--
-- Table structure for table `need_blood`
--

CREATE TABLE `need_blood` (
  `patient_name` varchar(255) DEFAULT NULL,
  `Doctor_name` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT 'cairo',
  `Address` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `select_Blood` varchar(255) NOT NULL DEFAULT 'A+',
  `unit_blood` int(255) DEFAULT NULL,
  `phone_num` varchar(255) NOT NULL,
  `select_gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `national_id` varchar(20) NOT NULL,
  `confirm_national_id` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `need_blood`
--

INSERT INTO `need_blood` (`patient_name`, `Doctor_name`, `city`, `Address`, `Email`, `select_Blood`, `unit_blood`, `phone_num`, `select_gender`, `national_id`, `confirm_national_id`) VALUES
('Hassan', 'Ahmed', 'Qalyub', 'test t', 'hasanm@gmail.com', 'O+', 2, '01005165414', 'Male', '1234567891234', '1234567891234');

-- --------------------------------------------------------

--
-- Table structure for table `reg_blood`
--

CREATE TABLE `reg_blood` (
  `FName` varchar(255) DEFAULT NULL,
  `LNme` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Gender` varchar(255) DEFAULT NULL,
  `national_id` varchar(20) NOT NULL,
  `confirm_national_id` varchar(20) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reg_blood`
--

INSERT INTO `reg_blood` (`FName`, `LNme`, `City`, `Address`, `Email`, `Gender`, `national_id`, `confirm_national_id`, `active`) VALUES
('Doaa', 'Mohamed', 'Cairo', 'test', 'test@gmail.com', 'Female', '1234567891231', '1234567891231', 0),
('Doaa2', 'Mohamed', 'مدينه نصر', 'شارع مصطفى محمود', 'test@gmail.com', 'Female', '1234567891232', '1234567891232', 1),
('Hassan', 'Elhawary', 'test', 'test t', 'test@gmail.com', 'Male', '1234567891234', '1234567891234', 1),
('Elhawary', 'Mohamed', 'شبرا مصر', 'test test ', 'hassan@gmail.com', 'Male', '1234567891235', '1234567891235', 0),
('Test', 'TEST', 'test', 'test test ', 'test@gmail.com', 'Female', '1234567891237', '1234567891237', 1);

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_esperanto_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_esperanto_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_esperanto_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_esperanto_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_esperanto_ci;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `full_name`, `email`, `title`, `description`) VALUES
(1, 'Hassan Mohamed', 'hassan@gmail.com', 'TEST title', 'description test test'),
(2, 'Hassan Mohamed', 'test@gmail.com', 'test', 'tsetsetsetsetsetse');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `need_blood`
--
ALTER TABLE `need_blood`
  ADD PRIMARY KEY (`national_id`);

--
-- Indexes for table `reg_blood`
--
ALTER TABLE `reg_blood`
  ADD PRIMARY KEY (`national_id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
